package com.hcl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hcl.model.Admin;
import com.hcl.model.UserInfo;
import com.hcl.util.Db;
import com.hcl.util.Query;

public class IAdminImpl implements IAdminDao {
	PreparedStatement pst;
	ResultSet rs;
	int result=0;


@Override
public int adminAuthentication(Admin admin) {
	//System.out.println("4");
	result=0;
	try {
	pst=Db.getConnection().prepareStatement(Query.adminAuth);
	pst.setString(1,admin.getUid());
	pst.setString(2,admin.getPassword());
	rs=pst.executeQuery();
	while(rs.next()) {
		result++;
	}
}catch(ClassNotFoundException | SQLException e) {
		System.out.println("Exception Occurs in Admin Authentication");
	}finally {
		try {
			pst.close();
			rs.close();
			Db.getConnection().close();
		}catch(ClassNotFoundException | SQLException e) {
			
		}
	}
	return result;
	}


@Override
public List<UserInfo> viewAllUser() {
	List<UserInfo> list=new ArrayList<UserInfo>();//list of objects
	try {
		pst=Db.getConnection().prepareStatement(Query.viewAll);
		rs=pst.executeQuery();
		while(rs.next()) {
			UserInfo info=new UserInfo(rs.getInt(1),rs.getString(2), rs.getString(3),
					rs.getString(4),rs.getString(5));//setter
			list.add(info);
		}
		} catch (ClassNotFoundException | SQLException e) {
	 System.out.println("Exception Occurs in View All User");	
	}finally {
		try {
			Db.getConnection().close();
			rs.close();
			pst.close();
		} catch (ClassNotFoundException | SQLException e) {
		}
	}
	return list;
}


@Override
public int addUser(UserInfo info) {
	result=0;
	try {
		pst=Db.getConnection().prepareStatement(Query.addUser);
		pst.setInt(1,info.getUid());
		pst.setString(2,info.getUname());
		pst.setString(3,info.getPassword());
		pst.setString(4,info.getFullname());
		pst.setString(5,info.getEmail());
		result=pst.executeUpdate();
	} catch (ClassNotFoundException | SQLException e) {
		System.out.println("Exception occurs in the add User");
	}finally {
		try {
			Db.getConnection().close();
			pst.close();
		}catch(ClassNotFoundException | SQLException e) {
			
		}
	}
	return result;
}


@Override
public int editName(UserInfo info) {
	result=0;
	try {
		pst = Db.getConnection().prepareStatement(Query.editName);
		pst.setString(1,info.getUname());
		pst.setInt(2,info.getUid());
		result=pst.executeUpdate();
	} catch (ClassNotFoundException | SQLException e) {
	System.out.println("Exception Occurs in Edit Name");	
	}
	return result;
}


@Override
public int editPasswordFullName(UserInfo info) {
	result=0;
	try {
		pst = Db.getConnection().prepareStatement(Query.editPasswordFullName);
		pst.setString(1,info.getPassword());
		pst.setString(2,info.getFullname());
		pst.setInt(3,info.getUid());
		result=pst.executeUpdate();
	} catch (ClassNotFoundException | SQLException e) {
	System.out.println("Exception Occurs in Edit Password FullName");	
	}
	return result;


}


@Override
public int editPasswordFullNameEmailName(UserInfo info) {
	result=0;
	try {
		pst = Db.getConnection().prepareStatement(Query.editPasswordFullNameEmailName);
		pst.setString(1,info.getPassword());
		pst.setString(2,info.getFullname());
		pst.setString(3,info.getEmail());
		pst.setString(4,info.getUname());
		pst.setInt(5,info.getUid());
		result=pst.executeUpdate();
	} catch (ClassNotFoundException | SQLException e) {
	System.out.println("Exception Occurs in Edit Password FullName Email Name");	
	}
	return result;
}


@Override
public int removeUser(UserInfo info) {
	result=0;
	try {
		pst = Db.getConnection().prepareStatement(Query.removeUser);
	    pst.setInt(1,info.getUid());
		result=pst.executeUpdate();
	} catch (ClassNotFoundException | SQLException e) {
	System.out.println("Exception Occurs in Remove User");	
	}finally {
		try {
			Db.getConnection().close();
			pst.close();
		}catch(ClassNotFoundException | SQLException e) {
			
		}
	}
	return result;
}



}



