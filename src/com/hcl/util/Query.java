package com.hcl.util;

public class Query {
  public static String adminAuth="select * from admin where uid=? and password=?";
  public static String viewAll="select * from user_info";
  public static String addUser="insert into user_info values(?,?,?,?,?)";
  public static String editName="update user_info set uname=? where uid=?";
  public static String editPasswordFullName="update user_info set password=?,fullname=? where uid=?";
  public static String editPasswordFullNameEmailName="update user_info set password=?,fullname=?,email=?,uname=? where uid=?";
  public static String removeUser="delete from user_info where uid=?";
  		
}


