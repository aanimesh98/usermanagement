package com.hcl.controller;



import java.util.List;

import com.hcl.dao.IAdminDao;
import com.hcl.dao.IAdminImpl;
import com.hcl.model.Admin;
import com.hcl.model.UserInfo;

public class AdminController {
	int result;
	IAdminDao dao=new IAdminImpl();
	
	
    public int adminAuthentication(String  uid,String password) {
    	Admin admin=new Admin(uid, password);
    	return dao.adminAuthentication(admin);
    	
    }
    public List<UserInfo> viewAllUser(){
    	return dao.viewAllUser();
    }
	public int addUser(int uid,String uname,String password,String fullname,String email) {
		UserInfo info=new UserInfo(uid,uname,password,fullname,email);
		return dao.addUser(info);
	}
	
	public int editName(String uname,int Uid) {
		UserInfo info = new UserInfo();
		info.setUname(uname);
		info.setUid(Uid);
		return dao.editName(info);
		
	}
	public int editPasswordFullName(String Password,String FullName,int Uid) {
		UserInfo info= new UserInfo();
		info.setPassword(Password);
		info.setFullname(FullName);
		info.setUid(Uid);
		return dao.editPasswordFullName(info);
		
	}
	public int editPasswordFullNameEmailName(String Password,String FullName,String Email,String uname,int Uid) {
		UserInfo info=new UserInfo(Uid, uname, Password, FullName, Email);
		return dao.editPasswordFullNameEmailName(info);
	}
	
	public int removeUser(int uid) {
		UserInfo info = new UserInfo();
		info.setUid(uid);
		return dao.removeUser(info);
		
	}
	
    
}

